---
Week: 44
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 44 - Programmering med SQL

Databaser er det sted hvor information ofte gemmes, det kan være en virksomheds regnskabssystem, emails, udviklingsdokumenter, oplysninger om ansatte og kunder, passwords etc.  
Databaser er derfor også vigtigt at beskytte imod angreb. Især ransomware angreb sigter mod at stjæle information fra virksomheder og ofte vil en usikker database kunne bruges af en hacker til at få oplyst passwordsog andre oplysninger der giver adgang til en virksomheds netværk og systemer.  

Hackere udnytter ofte svagheder i database implementation og applikationer derkommunikerer med databaser til at lave SQL injections.  
Til at lave SQL injections har hackeren både viden om at detektere og udnytte føromtalte svagheder.  
Hackeren bruger ofte værktøjer til at afdække hvilke svagheder appliaktioner og databaser har.  
Det kan også være at hackeren har skrevet sine egne værktøjer i f.eks Python.  

I denne uge skal du lære om SQL og hvordan det bruges i en Python appplikation, hvordan udviklere designer databaser og hvilke tiltag en udvikler kan tage for at beskytte en database.  
Du skal også lære om de forskellige typer af SQL injections, samt at bruge værktøjet SQLMap.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Opgaver gennemført

### Læringsmål

- Konstruere simple programmer der bruger SQL databaser
- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Afleveringer

- Ingen afleveringer

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- ..
