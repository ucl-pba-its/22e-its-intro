---
Week: 38
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 38 - Sikkerhed i netværksprotokoller

## Mål for ugen

Denne uge omhandler netværk og sikkerhed i netværk.
Emnerne belyses ved dels at arbejde med Wireshark for at lære at analysere mistænkelig netværkstrafik, dels ved at opsætte et lille netværk i vmware.
Vmware netværket udvides i næste uge med installation af en sårbar webapplikation. Målet med netværket er at den studerende har et lukket miljø til at træne i værktøjer der kan bruges i sikkerheds arbejde.
Endelig er der en Tryhackme opgave der introducerer netværks scannings værktøjet NMAP.

Der er en del arbejde i dagens opgaver og det er derfor vigtigt at i bruger alt den tid i har til rådighed i dag.

### Praktiske mål

- 2 xubuntu maskiner oprettet og konfigureret i vmware
- 1 vsrx konfigureret i vmware

### Læringsmål

- Mestre forskellige netværksanalyse tools (nmap, wireshark)
- Opsætte et simpelt netværk (vmware, vsrx, xubuntu)

## Afleveringer

- Ingen afleveringer, men de praktiske mål er en forudsætning for at kunne arbejde med næste uges opgaver.

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Besøg fra studievejleder |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- ..
