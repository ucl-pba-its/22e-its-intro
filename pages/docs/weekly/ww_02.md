---
Week: 37
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 37 - Grundlæggende netværksprotokoller

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Alle studerende deltager i oplæg fra Prueba
- THM: Introductory Networking gennemført
- THM: What is Networking? gennemført
- THM: Intro to LAN gennemført

### Læringsmål

- Den studerende ved hvad OSI modellen og dens lag er
- Den studerende kender TCP/IP modellen, dens lag og hvordan 3 way handshake fungerer
- Den studerende kan anvende ping, hvilken protokol ping anvender og relevante ping parametre
- Den studerende ved hvad traceroute kan anvendes til og hvordan det bruges
- Den studerende ved hvad whois er og kan anvende det via CLI
- Den studerende har viden om hvordan DNS systemet er opbygget og hvordan dig kan bruges til at forespørge DNS servere
- Den studerende ved hvad et netværk er herunder hvad IP adresser og MAC adresser er
- Den studerende kender til MAC spoofing
- Den studerende kender de forskellige netværks topologier
- Den studerende ved hvad hhv. en switch og en router anvendes til
- Den studerende har viden om ARP protokollen
- Den studerende kender til DHCP og hvordan protokollen fungerer


## Afleveringer

- ingen afleveringer denne uge

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Øvelser K1/K4          |
| 9:30  | Prueba oplæg          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- Vi har besøg fra Prueba og derfor vil mange af dagens opgaver skulle udføres uden underviser.  Det anbefales at i arbejder sammen i jeres team når i løser opgaverne, husk også at tage notater når i løser opgaver.
