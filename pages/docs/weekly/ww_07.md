---
Week: 43
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 43 - Programmering med netværk

Kendskab til hvordan netværk og internet tilgås fra en applikation er gode værktøjer i sikkerheds arbejdet.
Dels kan det bruges til at lave værktøjer der kan bruges i sikkerheds arbejdet, dels kan det bruges når tvivlsomme applikationer og scripts skal sikkerheds vurderes.

Denne uge i introduktion til it sikkerhed fokuserer på at implementere netværks kommunikation i Python, du skal i dybden med http(s) protokollen og du får også en godt indblik i tcp kommunikation.  
TShark er command line versionen af wireshark hvilket vil sige at du kan bruge TShark i scripts til f.eks at filtrere og analysere `.pcap` filer fra et angreb med TShark.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Opgaver gennemsført

### Læringsmål

- Konstruere simple programmer der kan bruge netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Afleveringer

- Password opgave fra uge 41

## Skema

Herunder er det planlagte program for ugen.

### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- Husk at tilmelde til besøg hos Energinet den 9. November, hvis du ønsker at deltage
