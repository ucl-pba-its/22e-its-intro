---
title: '22E ISOE221 - Introduktion til IT sikkerhed'
subtitle: 'Eksamen beskrivelse'
filename: '22E_ITS1_INTRO_eksamens_opgave'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2022-04-04
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamen beskrivelse
skip-toc: false
semester: 22S
---

# 22E ISOE221 - Introduktion til IT sikkerhed eksamen beskrivelse

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _introduktion til IT sikkerhed_.

# Eksamen beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit 5.2.2, side 17_ [https://esdhweb.ucl.dk/D22-1972441.pdf](https://esdhweb.ucl.dk/D22-1972441.pdf)

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern censur.

For hvert spørgsmål bør den studerende forberede en præsentation/oplæg på max 10 minutter.  
Efter den studerendes præsentation stiller eksaminator og censor spørgsmål.  
Oplæg og spørgsmål tager samlet 20 minutter, de sidste 5 minutter er afsat til votering.

# Eksamen spørgsmål

1. Netværk med fokus på OSI, TCP/IP modeller og netværksprotokoller samt sikkerhedsniveauer.
2. Netværk med fokus på trafikmonitorering (sniffing) og skanning.
3. Programmering i Python med database og fokus på database sikkerhed.
4. Scripting i bash og powershell med fokus på hvordan det kan anvendes i sikkerheds arbejdet
5. Programmering i Python med netværk og fokus på sikkerhed i protokoller.

# Eksamen datoer

- Forsøg 1 - 2022-11-15
- Forsøg 2 - 2022-12-01
- Forsøg 3 - 2023-01-12
